/*   test_declarations
     Author: cnngimenez

     Copyright (C) 2017 cnngimenez

     This program is free software: you can redistribute it and/or modify
     it under the terms of the GNU General Public License as published by
     the Free Software Foundation, either version 3 of the License, or
     at your option) any later version.

     This program is distributed in the hope that it will be useful,
     but WITHOUT ANY WARRANTY; without even the implied warranty of
     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
     GNU General Public License for more details.

     You should have received a copy of the GNU General Public License
     along with this program.  If not, see <http://www.gnu.org/licenses/>.

     17 ene 2017
*/


:- module(test_sqlite_model, []).
/** <module> test_sqlite_model: Tests for sqlite_model module.

Tests for declaration module.
*/


:- use_module('../prolog/fp/backend/sqlite_model').
:- use_module('../prolog/fp/backend/sqlite_common').
:- use_module(config).

:- use_module(library(prosqlite)).
:- use_module(library(db_facts)).

add_data(declarations_tbl(Pack, Name, Params, Desc, Line, File_path)) :-
    db_holds(
	declarations_tbl(Pack, Name, Params, Desc, Line, File_path)), !,
    %% red cut
    db_retractall(
	declarations_tbl(Pack, Name, Params, Desc, Line, File_path)),
    db_assert(
	declarations_tbl(Pack, Name, Params, Desc, Line, File_path)).

add_data(Dec) :-
    db_assert(Dec).

reset_db(LstDecs) :-
    test_dbconfig(Config),
	
    lang_db_filename(Config, DbFile),

    %% format('Deleting ~w DB file', [DbFile]),
    %% delete_file(DbFile),
    
    sqlite_connect(DbFile, database, exists(false)),
    (db_table(database, declarations_tbl), ! ; 
     db_create(database,
	       declarations_tbl(pack-text, name-text, params-text,
                                desc-text, line+int, file_path+text))),

    (db_table(database, files_tbl), ! ; 
     db_create(database,
	       files_tbl(pack+text, lastseen-timestamp))),

    db_disconnect(database),
    
    sqlite_connect(DbFile, database, exists(false)),
    %% db_retractall(declarations_tbl(_Pack, _Name, _Args, _Desc, _Line, _File)),

    maplist(add_data, LstDecs),
    
    db_disconnect(database).

   

:- begin_tests(sqlite_model).

test(create_db) :-
    test_dbconfig(DBConfig),
    
    create_db(DBConfig),

    lang_db_filename(DBConfig, SQLiteFile),
    exists_file(SQLiteFile).

delete_decs_file_setup :-
    test_filepath(2, File),
    
    reset_db(
	[
	    declarations_tbl(
		testpack, testdec, testparam, testdesc, 1, File),
	    declarations_tbl(
		testpack, testdec2, testparam2, testdesc2, 2, File),	    
	    declarations_tbl(
		testpack, testdec3, testparam3, testdesc3, 3, File),

	    files_tbl(File, 1)
	]).

/**
   It must remove all sqlite_model associated to a file.
*/
test(delete_decs_file, [setup(delete_decs_file_setup)]) :-
    test_dbconfig(Config),
    test_filepath(2, File),

    delete_decs_file(Config, File),

    %% Check if all corresponding declarations and files where deleted.
    lang_db_filename(Config, DbFile),
    sqlite_connect(DbFile, database),
    
    \+ db_holds(
	   declarations_tbl(
	       testpack, testdec, testparam, testdesc, 1, File)),
    \+ db_holds(
	    declarations_tbl(
		testpack, testdec2, testparam2, testdesc2, 2, File)),
    \+ db_holds(
	   declarations_tbl(
	       testpack, testdec3, testparam3, testdesc3, 3, File)),

    \+ db_holds(files_tbl(File, _)),

    sqlite_disconnect(database).

/**
if calling delete_decs_file/2 twice (when the File is not stored) it must
not fail.
*/
test(delete_decs_file_nonexistant) :-
    test_dbconfig(Config),
    test_filepath(2, File),

    delete_decs_file(Config, File),
    delete_decs_file(Config, File).


save_all_setup :-
    test_dbconfig(Config),
    test_filepath(1, File),
    
    lang_db_filename(Config, DbFile),
    sqlite_connect(DbFile, database),

    db_retractall(declarations_tbl(_, _, _, _, _, File)),
    db_retractall(files_tbl(File, _)).
    
    sqlite_disconnect(database).

test(save_all, [setup(save_all_setup)]) :-
    test_dbconfig(Config),
    test_filepath(1, File),

    save_all(
	Config, file(File, 1),
	[dec(save_all_pack, save_all_testdec, save_all_testparam,
	     save_all_testdesc, 1),
	 dec(save_all_pack, save_all_testdec2, save_all_testparam2,
	     save_all_testdesc2, 2),
	 dec(save_all_pack, save_all_testdec3, save_all_testparam3,
	     save_all_testdesc3, 3)
	]),

    %% Check if the declarations really exists on the DB.
    lang_db_filename(Config, DbFile),
    sqlite_connect(DbFile, database),
    
    db_holds(
	declarations_tbl(
	    save_all_pack, 
	    save_all_testdec, save_all_testparam,
	    save_all_testdesc, 1, File)),
    db_holds(
	   declarations_tbl(
	       save_all_pack, 
	       save_all_testdec2, save_all_testparam2,
	       save_all_testdesc2, 2, File)),
    db_holds(
	   declarations_tbl(
	       save_all_pack, 
	       save_all_testdec3, save_all_testparam3,
	       save_all_testdesc3, 3, File)),
    db_holds(files_tbl(File, _)),

    sqlite_disconnect(database).


:- end_tests(sqlite_model).
