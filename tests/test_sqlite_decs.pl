/*   test_sqlite_decs
     Author: cnngimenez.

     Copyright (C) 2020 cnngimenez

     This program is free software: you can redistribute it and/or modify
     it under the terms of the GNU General Public License as published by
     the Free Software Foundation, either version 3 of the License, or
     at your option) any later version.

     This program is distributed in the hope that it will be useful,
     but WITHOUT ANY WARRANTY; without even the implied warranty of
     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
     GNU General Public License for more details.

     You should have received a copy of the GNU General Public License
     along with this program.  If not, see <http://www.gnu.org/licenses/>.

     20 nov 2020
*/


:- module(test_sqlite_decs, [	      
	  ]).
/** <module> test_sqlite_decs: 


*/

:- use_module(config).
%% :- use_module('../prolog/fp/backend/sqlite_model').
:- use_module('../prolog/fp/backend/sqlite_common').
:- use_module('../prolog/fp/backend/sqlite_decs').

:- use_module(library(prosqlite)).
:- use_module(library(db_facts)).

:- begin_tests(sqlite_decs).

add_data(declarations_tbl(Pack, Name, Params, Desc, Line, File_path)) :-
    db_holds(
	declarations_tbl(Pack, Name, Params, Desc, Line, File_path)), !,
    %% red cut
    db_retractall(
	declarations_tbl(Pack, Name, Params, Desc, Line, File_path)),
    db_assert(
	declarations_tbl(Pack, Name, Params, Desc, Line, File_path)).

add_data(Dec) :-
    db_assert(Dec).

reset_db(LstDecs) :-
    test_dbconfig(Config),
	
    lang_db_filename(Config, DbFile),

    %% format('Deleting ~w DB file', [DbFile]),
    %% delete_file(DbFile),
    
    sqlite_connect(DbFile, database, exists(false)),
    (db_table(database, declarations_tbl), ! ; 
     db_create(database,
	       declarations_tbl(pack-text, name-text, params-text,
                                desc-text, line+int, file_path+text))),
    db_disconnect(database),
    
    sqlite_connect(DbFile, database, exists(false)),
    %% db_retractall(declarations_tbl(_Pack, _Name, _Args, _Desc, _Line, _File)),

    maplist(add_data, LstDecs),
    
    db_disconnect(database).

/**
   declaration_setup is det

Create the database, add the second file on test_filepath/2 and insert some
test sqlite_model into the database.	
*/
declaration_setup :-
    test_filepath(2, File),
    reset_db(
	[
	    declarations_tbl(
		testpack, testdec, testparam, testdesc, 1, File),
	    declarations_tbl(
		testpack, testdec2, testparam2, testdesc2, 2, File),	    
	    declarations_tbl(
		testpack, testdec3, testparam3, testdesc3, 3, File)
	]).

test(declaration, [setup(declaration_setup)]) :-
    test_dbconfig(Config),
    test_filepath(2, File),
    declaration(Config, testpack, testdec, testparam, testdesc, 1, File).

%% We cannot test for nondeterministic behaviour.
%% Each declaration/6 call opens and closes the database providing
%% the same answer or none after the first call.
%%
%% test(declaration_nondet, [setup(declaration_setup),
%% 			  all(dec(Name, Param, Desc, Line) == [
%% 				  dec(testdec, testparam, testdesc, 1),
%% 				  dec(testdec2, testparam2, testdesc2, 2),
%% 				  dec(testdec3, testparam3, testdesc3, 3)])
%% 			 ]) :-
%%     test_filepath(2, File),
%%     test_dbconfig(Config),
%%     declaration(Config, Name, Param, Desc, Line, File).
    
test(all_declarations, [setup(declaration_setup)]) :-
    test_dbconfig(Config),
    test_filepath(2, File),

    all_declarations(Config,
		     _Pack, _Name, _Params, _Desc, _Line, File,
		     Results),

    Results == [
	dec(testpack, testdec, testparam, testdesc, 1, File),
	dec(testpack, testdec2, testparam2, testdesc2, 2, File),
	dec(testpack, testdec3, testparam3, testdesc3, 3, File)
    ].

starting_with_setup :-
    test_filepath(1, File),

    reset_db(
	[
	    declarations_tbl(
		testpack,
		starting_with_testdec, starting_with_testparam,
		starting_with_testdesc, 1, File),
	    declarations_tbl(
		testpack,
		starting_with_testdec2, starting_with_testparam2,
		starting_with_testdesc2, 2, File),
	    declarations_tbl(
		testpack,
		starting_with_testdec3, starting_with_testparam3,
		starting_with_testdesc3, 3, File)
	]).
    
test(starting_with, [setup(starting_with_setup)]) :-
    test_dbconfig(Config),
    test_filepath(1, File),

    starting_with(Config, 'starting', Results),

    Results == [
	dec(testpack,
	    starting_with_testdec, starting_with_testparam,
	    starting_with_testdesc, 1, File),
	dec(testpack,
	    starting_with_testdec2, starting_with_testparam2,
	    starting_with_testdesc2, 2, File),
	dec(testpack,
	    starting_with_testdec3, starting_with_testparam3,
	    starting_with_testdesc3, 3, File)
    ].

test(has_substring, [setup(starting_with_setup)]) :-
    test_dbconfig(Config),
    test_filepath(1, File),

    has_substring(Config, 'with', Results),
    Results == [
	dec(testpack,
	    starting_with_testdec, starting_with_testparam,
	    starting_with_testdesc, 1, File),
	dec(testpack,
	    starting_with_testdec2, starting_with_testparam2,
	    starting_with_testdesc2, 2, File),
	dec(testpack,
	    starting_with_testdec3, starting_with_testparam3,
	    starting_with_testdesc3, 3, File)
    ].

:- end_tests(sqlite_decs).

