/*   test_code_scanner
     Author: cnngimenez.

     Copyright (C) 2017 cnngimenez

     This program is free software: you can redistribute it and/or modify
     it under the terms of the GNU General Public License as published by
     the Free Software Foundation, either version 3 of the License, or
     at your option) any later version.

     This program is distributed in the hope that it will be useful,
     but WITHOUT ANY WARRANTY; without even the implied warranty of
     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
     GNU General Public License for more details.

     You should have received a copy of the GNU General Public License
     along with this program.  If not, see <http://www.gnu.org/licenses/>.

     17 ene 2017
*/


:- module(test_code_scanner, []).
/** <module> test_code_scanner: Tests for code_scanner module.

Tests for code_scanner module.
*/

:- begin_tests(code_scanner).
:- use_module('../prolog/fp/code_scanner').
:- use_module(library(fp/dcgs/prologlang)).

test(filter_empty_decs) :-
    code_scanner:filter_empty_decs(
		     [dec("A", "", "", 1),
		      dec("B", "B", "B", 2),
		      dec("C", "", "C", 3),
		      dec("D", "D", "", 4)],
		     Result),
    Result == [dec("B", "B", "B", 2),
	      dec("C", "", "C", 3),
	      dec("D", "D", "", 4)].



test(scan_file) :-
    scan_file(prologlang, './examples/example_test.pl', Decs),
    Decs = [
	dec("module","config, [\n\t      test_dbconfig/1,\n\t      test_path/1,\n\t      test_filepath/2\n\t  ]","",23),
	dec("test_dbconfig","-DBConfig:atom","",34),
	dec("test_path","'/opt/fp/src'","\n   test_path(-Path:term).\n\nChange this configuration to where you have the \"db\" directory where the SQLite database should be.\n\n@param Path A real path to the base directory where the database \"db\" directory is. \n",40),
	dec("test_filepath","+Num:int, -FilePath:term","",50),
	dec("test_filepath","1, './config.pl'","",58),
	dec("test_filepath","2, 'another_file.adb'","",58),
	dec("test_filepath","3, './test_queries.pl'","",59)
    ].

test(scan_file_b) :-
    scan_file(ada, './examples/example.adb', Result),
    %% write(Result),
    Result = [
        package("My_Package.Another_Package.Example", 22),
        dec("Append_Strings",
            "Prefix, Suffix : String; Appened : out String", "", 26),
        dec("Append_Fnc", "Prefix, Suffix : String", "", 32)        
    ].

/**
   We let scan_next to be nondeterministic, because the DCGs implemented can be nondet too.
*/
test(scan_next, [nondet]) :-
    String = `/** \ncomments\n\n*/\ntestpred(A) :- true.\n\n%% more comments`,
    scan_next(subprog_prologlang, String,
	      dec("testpred", "A", "\ncomments\n\n", 1),
	      `\n\n%% more comments`, 1, 5, 5).

:- end_tests(code_scanner).
