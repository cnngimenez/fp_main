/*   test_scanner
     Author: cnngimenez.

     Copyright (C) 2017 cnngimenez

     This program is free software: you can redistribute it and/or modify
     it under the terms of the GNU General Public License as published by
     the Free Software Foundation, either version 3 of the License, or
     at your option) any later version.

     This program is distributed in the hope that it will be useful,
     but WITHOUT ANY WARRANTY; without even the implied warranty of
     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
     GNU General Public License for more details.

     You should have received a copy of the GNU General Public License
     along with this program.  If not, see <http://www.gnu.org/licenses/>.

     17 ene 2017
*/


:- module(test_scanner, []).
/** <module> test_scanner: Tests for scanner module.

Tests for scanner module.
*/

:- use_module(config).
:- use_module('../prolog/fp/scanner').
:- use_module('../prolog/fp/backend/sqlite_model').
:- use_module('../prolog/fp/backend/sqlite_decs').
:- use_module('../prolog/fp/backend/sqlite_files').

initialize_fp_dir :-
    test_path(Path), test_dbconfig(DBConfig),
    nl, format('- Using path: ~w\n Configure test_path/1 to point to the "db" directory if this is not correct.', [Path]),
    nl, format('- Using DBConfig: ~w\n Change test_dbconfig/1 if language and project name is not correct.', [DBConfig]),
    nl.

:- begin_tests(scanner, [setup(initialize_fp_dir)]).

scan_setup :-
    create_db(Config),
    ExampleFile = './examples/example_test.pl',
    absolute_file_name(ExampleFile, StoredFile),

    delete_decs_file(Config, StoredFile).

test(scan, [setup(scan_setup)]) :-
    test_dbconfig(Config),
    ExampleFile = './examples/example_test.pl',

    scan(Config, ExampleFile),

    %%  Check if the correct declarations where added.
    absolute_file_name(ExampleFile, AbsFile),

    declaration(
	Config,
	Pack, 'test_dbconfig', '-DBConfig:atom', '', 34, AbsFile),
    write(Pack),
    declaration(Config,
		Pack, 'test_path', A, _Desc,40, AbsFile),
    A == '\'/opt/fp/src\'',

    declaration(
	Config,
	Pack, 'test_filepath','+Num:int, -FilePath:term','', 50,AbsFile),

    declaration(
	Config,
	Pack, 'test_filepath', B, '', 58, AbsFile),
    B == '1, \'./config.pl\'',

    declaration(
	Config,
	Pack, 'test_filepath', C, '', 59, AbsFile),
    C == '3, \'./test_queries.pl\''.


test(scan_nosave) :-
    scan_nosave(prologlang, './examples/example_test.pl', Result),

    Result = [
	dec("", "module", _Params, "", 23),
	dec("", "test_dbconfig", "-DBConfig:atom", "", 34),
	dec("", "test_path", "'/opt/fp/src'", _Desc, 40),
	dec("", "test_filepath", "+Num:int, -FilePath:term", "", 50),
	dec("", "test_filepath", "1, './config.pl'", "", 58),
	dec("", "test_filepath", "2, 'another_file.adb'", "", 58),
	dec("", "test_filepath", "3, './test_queries.pl'", "", 59)
    ].

%% test(scan_recursive) :-

:- end_tests(scanner).

