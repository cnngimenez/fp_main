/*   test_sqlite_common.pl
     Author: cnngimenez.

     Copyright (C) 2020 cnngimenez

     This program is free software: you can redistribute it and/or modify
     it under the terms of the GNU General Public License as published by
     the Free Software Foundation, either version 3 of the License, or
     at your option) any later version.

     This program is distributed in the hope that it will be useful,
     but WITHOUT ANY WARRANTY; without even the implied warranty of
     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
     GNU General Public License for more details.

     You should have received a copy of the GNU General Public License
     along with this program.  If not, see <http://www.gnu.org/licenses/>.

     20 nov 2020
*/


:- module(test_sqlite_common, [	      
	  ]).
/** <module> test_sqlite_common: 


*/

:- use_module(config).
:- use_module('../prolog/fp/backend/sqlite_common').

:- begin_tests(sqlite_common).

test(lang_db_filename) :-
    test_dbconfig(Config),
    lang_db_filename(Config, SQLiteFile),

    %% format('SQLite database full path: ~w\n', [SQLiteFile]),
    
    test_path(Path),    
    format(atom(SQLiteFile), '~w/dbs/prologlang-testproject.sqlite', [Path]).

:- end_tests(sqlite_common).

