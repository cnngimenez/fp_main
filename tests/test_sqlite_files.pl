/*   test_sqlite_files.pl
     Author: cnngimenez.

     Copyright (C) 2020 cnngimenez

     This program is free software: you can redistribute it and/or modify
     it under the terms of the GNU General Public License as published by
     the Free Software Foundation, either version 3 of the License, or
     at your option) any later version.

     This program is distributed in the hope that it will be useful,
     but WITHOUT ANY WARRANTY; without even the implied warranty of
     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
     GNU General Public License for more details.

     You should have received a copy of the GNU General Public License
     along with this program.  If not, see <http://www.gnu.org/licenses/>.

     20 nov 2020
*/

:- module(test_sqlite_files, []).

:- use_module(config).
:- use_module('../prolog/fp/backend/sqlite_common').
:- use_module('../prolog/fp/backend/sqlite_files').

:- use_module(library(prosqlite)).
:- use_module(library(db_facts)).

:- begin_tests(sqlite_files).

add_data(files_tbl(File, Date)) :-
    db_holds(files_tbl(File, _)), !,
    db_retractall(files_tbl(File, _)),
    db_assert(files_tbl(File, Date)).

add_data(Dec) :-
    db_assert(Dec).

reset_db(LstDecs) :-
    test_dbconfig(Config),
	
    lang_db_filename(Config, DbFile),

    %% format('Deleting ~w DB file', [DbFile]),
    %% delete_file(DbFile),
    
    sqlite_connect(DbFile, database, exists(false)),
    (db_table(database, files_tbl), ! ; 
     db_create(database,
	       files_tbl(pack+text, lastseen-timestamp))),
    db_disconnect(database),
    
    sqlite_connect(DbFile, database, exists(false)),
    %% db_retractall(declarations_tbl(_Pack, _Name, _Args, _Desc, _Line, _File)),

    maplist(add_data, LstDecs),
    
    db_disconnect(database).


test(add_file) :-
    test_filepath(1, File), test_filepath(2, File2),

    test_dbconfig(DBConfig),
    
    %% testing adding the same file twice: it must not fail
    add_file(DBConfig, File, 1),
    add_file(DBConfig, File, 1),
    %% testing a new file
    add_file(DBConfig, File2, 2),
    
    %% tests their existence in the DB
    lang_db_filename(DBConfig, DbFile),
    sqlite_connect(DbFile, database, exists(false)),
    db_holds(files_tbl(File, 1)),
    db_holds(files_tbl(File2, 2)),
    sqlite_disconnect(database).

file_setup :-
    test_filepath(1, File),
    reset_db([
		    files_tbl(File, 1)
		]).

test(file, [setup(file_setup)]) :-
    test_filepath(1, File),
    test_dbconfig(DBConfig),

    %% test by date and filename
    file(DBConfig, File, 1),
    
    %% test for nonexistence
    \+ file(DBConfig, 'file_that_doesnot_exists', _).

%% It cannot became nondeteministic, db_holds/1 cannot work properly as 
%% soon as the SQLite is disconnected.
%%
%% test('file nondeterministic', [all(Number == [1,2])]) :-
%%     test_filepath(1, File), test_filepath(2, File2),
%%     test_dbconfig(DBConfig),
%%     add_file(DBConfig, File, 1),
%%     add_file(DBConfig, File2, 2),
%%
%%     %% test filename existence
%%     file(DBConfig, _, Number).



/**
   Create the database and add three files.
   Also, deletes any files stored before.
*/
all_files_setup :-
    test_filepath(1, F1),
    test_filepath(2, F2),
    test_filepath(3, F3),

    reset_db([
		    files_tbl(F1, 1),
		    files_tbl(F2, 2),
		    files_tbl(F3, 3)
		]).

test(all_files, [setup(all_files_setup)]) :-
    test_dbconfig(Config),

    all_files(Config, Results),

    %% Check if the elements are fine.
    test_filepath(1, F1),
    test_filepath(2, F2),
    test_filepath(3, F3),
    
    length(Results, 3),

    member(file(F1, 1), Results),!, %% member leaves with choicepoint!
    member(file(F2, 2), Results),!,
    member(file(F3, 3), Results),!.

test(delete_file, [setup(all_files_setup)]) :-
    test_dbconfig(Config),
    test_filepath(2, File),

    delete_file(Config, File, _),

    %% Check that the File actually has been deleted on DB.
    lang_db_filename(Config, DbFile),
    sqlite_connect(DbFile, database, exists(false)),
    \+ db_holds(files_tbl(File, _)),
    sqlite_disconnect(database).

/**
 Calling delete_file/3 twice must not fail.
*/
test(delete_file_nonexistent) :-
    test_dbconfig(Config),
    test_filepath(2, File),

    delete_file(Config, File, _),
    delete_file(Config, File, _).
    

/**
Add two files: one with the 
*/
file_unmodified_setup :-
    test_filepath(1, File),
    test_filepath(3, File2),

    time_file(File, Filetime),

    reset_db([
		    files_tbl(File, Filetime),
		    files_tbl(File2, 1)
		]).

test(file_unmodified,[setup(file_unmodified_setup)]) :-
    test_dbconfig(Config),
    test_filepath(1, File),
    test_filepath(3, File2),

    %% File exists and has not been modified.
    file_unmodified(Config, File),
    %% File exists and has been modified.
    \+ file_unmodified(Config, File2).


:- end_tests(sqlite_files).
