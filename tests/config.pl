/*   config.pl
     Author: cnngimenez.

     Copyright (C) 2020 cnngimenez

     This program is free software: you can redistribute it and/or modify
     it under the terms of the GNU General Public License as published by
     the Free Software Foundation, either version 3 of the License, or
     at your option) any later version.

     This program is distributed in the hope that it will be useful,
     but WITHOUT ANY WARRANTY; without even the implied warranty of
     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
     GNU General Public License for more details.

     You should have received a copy of the GNU General Public License
     along with this program.  If not, see <http://www.gnu.org/licenses/>.

     20 nov 2020
*/

:- module(config, [
	      test_dbconfig/1,
	      test_path/1,
	      test_filepath/2
	  ]).
/** <module> config: Configuration file for the tests.


*/

/**
   test_dbconfig(-DBConfig:atom).

Database language and project name for the testing suite.
*/
test_dbconfig(database(prologlang, testproject)).

/**
   test_path(-Path:term).

Change this configuration to where you have the "dbs" directory where the
SQLite database should be.

@param Path A real path to the base directory where the database "dbs"
  directory is. 
*/
test_path(Path) :-
    expand_file_name('~/.config/fp/', [Path]).

/**
   test_filepath(+Num:int, -FilePath:term).

Maps between a number and a file path.

* 1 and 3 : a file that already exists.
* 2 : A file that may not exists.

*/
test_filepath(1, './config.pl').
test_filepath(2, 'another_file.adb').
test_filepath(3, './test_queries.pl').
