--  example.adb ---

--  Copyright 2020 cnngimenez
--
--  Author: cnngimenez

--  This program is free software: you can redistribute it and/or modify
--  it under the terms of the GNU General Public License as published by
--  the Free Software Foundation, either version 3 of the License, or
--  (at your option) any later version.

--  This program is distributed in the hope that it will be useful,
--  but WITHOUT ANY WARRANTY; without even the implied warranty of
--  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
--  GNU General Public License for more details.

--  You should have received a copy of the GNU General Public License
--  along with this program.  If not, see <http://www.gnu.org/licenses/>.

-------------------------------------------------------------------------

package body My_Package.Another_Package.Example is

    A_Variable : A_Type;

    procedure Append_Strings (Prefix, Suffix : String;
                              Appened : out String) is
    begin
        Append := Prefix & Suffix;
    end Append_String;

    function Append_Fnc (Prefix, Suffix : String) return String is
    begin
        return Prefix & Suffix;
    end Append_Fnc;

end My_Package.Another_Package.Example;
