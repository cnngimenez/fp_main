/*   fp_scanfile.pl
     Author: Giménez, Christian.

     Copyright (C) 2020 Giménez, Christian

     This program is free software: you can redistribute it and/or modify
     it under the terms of the GNU General Public License as published by
     the Free Software Foundation, either version 3 of the License, or
     at your option) any later version.

     This program is distributed in the hope that it will be useful,
     but WITHOUT ANY WARRANTY; without even the implied warranty of
     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
     GNU General Public License for more details.

     You should have received a copy of the GNU General Public License
     along with this program.  If not, see <http://www.gnu.org/licenses/>.

     22 nov 2020
*/

:- module(fp_scanfile, []).
:- use_module('../prolog/fp/scanner').
:- use_module(library(prolog_stack)).

print_dec(package(Pack, Line)) :-
    format('~d | Package: "~w"', [Line, Pack]), nl, !.
print_dec(dec(Pack, Name, Params, Desc, Line)) :-
    format('~d | ~w || ~w || ~w ||: ~w ||', [Line, Pack, Name, Params, Desc]), nl.

print_decs(LstDecs) :-
    maplist(print_dec, LstDecs).


main(['--nosave', Language, File]) :- !, %% red cut
    scan_nosave(Language, File, LstDecs), !,

    print_decs(LstDecs).

main([Project, Language, File]) :- !, %% red cut
    format('File: ~w. Language: ~w. Project: ~w',
	   [File, Language, Project]), nl,
    exists_file(File),
    write('File exists. Scanning...'), nl,
    catch_with_backtrace(scan(database(Language, Project), File),
			  Error,
			  print_message(error, Error)),
    write(Error),
    nl,
    write('Scan succesful').

main(_Argv) :-   
    write("Synopsis:"), nl,
    write("    fp_scanfile LANGUAGE FILE_TO_SCAN").

:- initialization(main, main).
