# FP - Main libraries
Scan your code files for subprograms declarations (functions, procedures, predicates, etc.).

# Requirements
This module requires the following SWI libraries:

- prosqlite
- db_facts

Install inside the SWI REPL:

```prolog
?- pack_install(db_facts).
?- pack_install(prosqlite).
```

The `fp_dcgs` add-on is required to provide language support, install with:

```prolog
?- pack_install(fp_dcgs).
```

For more information about adding language support, see the [fp_dcgs](https://gitlab.com/cnngimenez/fp_dcgs) repository.

# Usage Example

```prolog
?- use_module(fp/queries).
true.

?- initialize('/home/user/fp', database('php','project')).
true.

?- scan_recursive(database('php','project'), '/home/user/project-dir/', '*.php').
% Scanning results are displayed here.
true.

?- starting_with(database('php','project'), 'foo').
% Subprograms starting with "foo" are listed here.
true.

```

The SQLite databases, resides at ~/.local/share/fp/dbs/*.sqlite by default.


# Running Tests

Load the modules inside the tests directory and run the tests using `run_tests.`.


See [Prolog Unit Tests package documentation](http://www.swi-prolog.org/pldoc/doc_for?object=section(%27packages/plunit.html%27)) for more information.
