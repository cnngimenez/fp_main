/*   scanner
     Author: cnngimenez.

     Copyright (C) 2016 cnngimenez

     This program is free software: you can redistribute it and/or modify
     it under the terms of the GNU General Public License as published by
     the Free Software Foundation, either version 3 of the License, or
     at your option) any later version.

     This program is distributed in the hope that it will be useful,
     but WITHOUT ANY WARRANTY; without even the implied warranty of
     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
     GNU General Public License for more details.

     You should have received a copy of the GNU General Public License
     along with this program.  If not, see <http://www.gnu.org/licenses/>.

     19 ago 2016
*/


:- module(scanner, [
	      scan/2,
	      scan_nosave/3,
	      scan_recursive/3
	  ]).
/** <module> scanner: Scanner main function.

Here lays the main function for scan files.
*/

:- use_module(code_scanner).
:- use_module(backend/sqlite_model).
:- use_module(backend/sqlite_files).
:- use_module(library(ansi_term)).

:- use_module(library(lists)).

/**
   show_mem_status.

Display on the current_output/1 the memory status.
*/
show_mem_status :-
    prolog_stack_property(global, limit(GlobalLimit)),
    statistics(global_stack, [GlobalUse, GlobalFree]),
    prolog_stack_property(trail, limit(TrailLimit)),
    statistics(trail, [TrailUse, TrailFree]),
    prolog_stack_property(local, limit(LocalLimit)),
    statistics(local_stack, [LocalUse, LocalFree]),
    ansi_format([faint], 'G:~w/~w(~w) L:~w/~w(~w) T:~w/~w(~w)', [
		    GlobalUse,GlobalLimit,GlobalFree,
		    LocalUse, LocalLimit, LocalFree,
		    TrailUse, TrailLimit, TrailFree]),flush_output,!.

/** scan(+DbConfig:term, +File:string).

Scan the given File considering the Language grammar.
*/
scan(database(Language, ProjName), File) :-
    nl, format('~w:~w --------->', [Language, ProjName]),
    absolute_file_name(File, FileAbs),
    create_db(database(Language, ProjName)),
    scan_int(database(Language, ProjName), File, FileAbs).

scan_int(database(Language, ProjName), File, FileAbs) :-
    file_unmodified(database(Language, ProjName), FileAbs),!,
    nl, display(File),
    nl, ansi_format([fg(green)], 'Scanned and not modified.', []), flush_output.

scan_int(database(Language, ProjName), File, FileAbs) :-
    \+ file_unmodified(database(Language, ProjName), FileAbs),!,
       nl, display(File), flush_output,
    scan_file(Language, FileAbs, LstDecs), !,

    nl, length(LstDecs, Amount), display(Amount), display(' declarations processed.'),
    add_pack_to_decs(LstDecs, Lst1),
    to_terms(Lst1, Lst2),

    nl, display('Removing all previous declarations from file.'), flush_output,
    (delete_decs_file(database(Language, ProjName), FileAbs),! ; true,!),

    nl, display('Saving new declarations.'), flush_output,
    time_file(FileAbs, Time),
    save_all(database(Language, ProjName), file(FileAbs, Time), Lst2),

    nl, ansi_format([fg(green), bold], 'Declarations saved. Done.', []), flush_output.

/** scan_list(+DbConfig:term, +LstFile:list).

Scan a list of file using scan/2 on each one.
*/
scan_list(DbConfig, LstFile) :-
    length(LstFile, Amount),
    scan_list_int(DbConfig, LstFile, Amount).

scan_list_int(_DbConfig, [], _Amount).
scan_list_int(DbConfig, [File | Rest], Amount) :-
    nl, format('Scaning file... Remains ~w files ', [Amount]), nl, flush_output,
    nl, show_mem_status, nl,
    scan(DbConfig, File),
    Amount2 is Amount - 1,
    scan_list_int(DbConfig, Rest, Amount2).

/** scan_recursive(+DbConfig:term, +Path:term, +Wildcard:Term).

Scan all the files thats match the given Wildcard through all the directories
inside Path and its subdirectories too. 
*/
scan_recursive(DbConfig, Path, Wildcard) :-
    clean_nonexistent_files(DbConfig, LstDeletedFiles),
    ansi_format([fg(green), bold], 'Removing non-existent files from DB',[]),nl,
    write(LstDeletedFiles),nl,
    
    absolute_file_name(Path, PathAbs),
    search_dirs(Wildcard, PathAbs, LstFiles),
    scan_list(DbConfig, LstFiles),!.

/** scan_nosave(+Language:term, +File:term, -LstDecs).

true iff LstDec is a list of dec/2 terms with declarations obtained from the
given File matching the subprog from the Language setted.
*/
scan_nosave(Language, File, LstDecs) :-
    absolute_file_name(File, FileAbs),
    scan_file(Language, FileAbs, LstDecsUnfiltered),
    filter_empty_decs(LstDecsUnfiltered, LstDecs1),
    add_pack_to_decs(LstDecs1, LstDecs).
    %% display(LstDecs).


/** add_pack_to_decs(+LstDecs: list, -LstOut: list)

Add a package argument to each dec/4 term. The PackageName which is used to
fill the argument is the package/1 term founded on the list before the dec/4
terms in the same list. Also, remove all package/1 term in the list.

For example:
```
?- add_pack_to_decs([
  term("hello", "world", "desc", 10), 
  package("pack"), 
  term("term2", "args2", "desc2", 20)
], Out).
```

It returns the following result:
```
Out = [
  term("pack", "hello", "world", "desc", 10), 
  term("pack", "term2", "args2", "desc2", 20)
]
```
*/
add_pack_to_decs(LstDecs, LstOut) :-
    add_pack_to_decs_int("", LstDecs, LstOut).

add_pack_to_decs_int(_PackageName, [], []).
add_pack_to_decs_int(_OldPackageName,
                     [package(PackageName, _)|Rest], RestOut) :- !,
    add_pack_to_decs_int(PackageName, Rest, RestOut).
add_pack_to_decs_int(PackageName, [DecIn|Rest], [DecOut|RestOut]) :-
    add_pack_to_dec(PackageName, DecIn, DecOut), !, %% red cut
    add_pack_to_decs_int(PackageName, Rest, RestOut).

add_pack_to_dec(PackageName,
                dec(Name, Arg, Desc, Line),
                dec(PackageName, Name, Arg, Desc, Line)).
 
to_terms([], []).
to_terms([dec(PackS, NameS, ArgsS, DescS,Line)|R], [dec(PackA, NameA, ArgsA, DescA, Line)|R2]) :-
    atom_string(PackA, PackS),
    atom_string(NameA, NameS),
    atom_string(ArgsA, ArgsS),
    atom_string(DescA, DescS),
    to_terms(R, R2).


/** take_files(+Wildcard:term, +Path:term, -LstFiles:list).

true iff LstFiles is a list of files paths from the Path directory where 
Wildcard matchs. 

@see expand_file_name/2.
*/
take_files(Wildcard, Path, LstFiles) :- 
    atomic_list_concat([Path, '/', Wildcard], Wc),
    expand_file_name(Wc, LstFiles).


/** use_absolutes(+Relative, ?LstRels, ?LstAbs).

true iff all the paths in LstRels are the same but absolute paths on LstAbs.

Use Relative for a relative path of the files in LstRels.
*/
use_absolutes(_Rel, [], []).
use_absolutes(Rel, [File|Rest], [Abs| Rest2]) :-
    atomic_list_concat([Rel, '/', File], File2),
    absolute_file_name(File2, Abs),
    use_absolutes(Rel, Rest, Rest2).

/** remove_pointsdirs(?LstFiles, ?LstFiles2).

true iff LstFiles2 is LstFiles without '.' and '..' terms.
*/
remove_pointsdirs([], []).
remove_pointsdirs(['.'|L], Lst2) :- !,
    remove_pointsdirs(L, Lst2).
remove_pointsdirs(['..'|L], Lst2) :- !,
    remove_pointsdirs(L, Lst2).
remove_pointsdirs([D|L], [D|Lst2]) :-
    remove_pointsdirs(L, Lst2).


/** get_abs_files(?Path, ?Lst).

true iff Lst are the files without '.' and '..' and with absolute path from 
Path.
*/
get_abs_files(Path, Lst) :-
    directory_files(Path, Lst1),
    remove_pointsdirs(Lst1, Lst2),
    use_absolutes(Path, Lst2, Lst).


/** search_dirs(+Path, -LstDirs).

true if LstDirs is alist with all the directories and subdirectories 
recursivelly from Path all of them in absolute path.
*/
search_dirs(Wildcard, Path, LstFiles) :-
    absolute_file_name(Path, AbsPath),
    search_dirs_int(Wildcard, [AbsPath], LstFiles).

search_dirs_int(_Wildcard, [], []).
search_dirs_int(Wildcard, [Path|Rest], LstFiles) :- 
    exists_directory(Path), 
    get_abs_files(Path, Lst3),
    append(Lst3, Rest, LstAll),
    take_files(Wildcard, Path, LstF1), %% Take the files from this directory.
    search_dirs_int(Wildcard, LstAll, LstF2),
    append(LstF1, LstF2, LstFiles).
search_dirs_int(Wildcard, [Path|Rest], LstFiles) :-
    \+ exists_directory(Path),
    search_dirs_int(Wildcard, Rest, LstFiles).

/**
   check_files_existence(+LstFiles:list, -LstExistentFiles:list, -LstNonExistentFiles:list) is det

Check all the filepath on the LstFiles list. Return all filepath's that doesn't exists on LstNonExistentFiles and all that exists on LstExistentFiles.

@param LstFiles A list of file/2 terms ( `file(Path:atom, Date:int)` ).
@param LstExistentFiles A list of file/2 terms of the files that exists on the file system.
@param LstNonExistentFiles A list of file/2 terms of the files that doesn't exist on the file system.
*/
check_files_existence([], [], []).
check_files_existence([file(File, Date)|Rest], [file(File, Date)|Rest2], Rest3) :-
    exists_file(File),!, % redcut
    check_files_existence(Rest, Rest2, Rest3).
check_files_existence([File|Rest], Rest2, [File|Rest3]) :-
    check_files_existence(Rest, Rest2, Rest3).

/**
   delete_files(+DBConfig:term, +LstFiles:list, -LstDeletedFiles:list) is det

Delete all files and its declarations on the DB. **No files from the file system will be deleted!**

@param LstFiles a list of file/2 terms `file(Path:term, Date:int)`.
@param LstDeletedFiles a list of terms with the paths succesfully deleted from the DB.
*/
delete_files(_DBConfig, [], []).
delete_files(DBConfig, [file(File, _Date)|Rest], [File|Rest2]) :-
    delete_decs_file(DBConfig, File),!, %redcut
    delete_files(DBConfig, Rest, Rest2).
delete_files(DBConfig, [file(_File, _Date)|Rest], Rest2) :-
    %% something wrong happened on delete_decs_file/2.
    delete_files(DBConfig, Rest, Rest2).
    
/**
   clean_nonexistent_files(+DBConfig:term) is det

Remove from the DB those files that doesn't exists in the file system.
*/
clean_nonexistent_files(DBConfig, LstDeletedFiles) :-
    all_files(DBConfig, LstFiles),
    check_files_existence(LstFiles, _, LstDelFiles),
    delete_files(DBConfig,LstDelFiles, LstDeletedFiles).
