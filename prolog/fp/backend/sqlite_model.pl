/*   sqlite_model.pl
     Author: cnngimenez

     Copyright (C) 2016 cnngimenez.

     This program is free software: you can redistribute it and/or modify
     it under the terms of the GNU General Public License as published by
     the Free Software Foundation, either version 3 of the License, or
     at your option) any later version.

     This program is distributed in the hope that it will be useful,
     but WITHOUT ANY WARRANTY; without even the implied warranty of
     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
     GNU General Public License for more details.

     You should have received a copy of the GNU General Public License
     along with this program.  If not, see <http://www.gnu.org/licenses/>.

     16 ago 2016
*/


:- module(sqlite_model, [
	      create_db/1,
	      save_all/3,
	      delete_decs_file/2
	  ]).
/** <module> sqlite_model: Basic DB predicates.
Provides the basic predicates and its access to the SQLite DB.

It is recommendend that a language is stored per each SQLite database file. For
example, LaTeX macros is stored as declarations in an SQLite file called
`latex.sqlite`, C functions at a `c.sqlite`, etc.

Different project can be stored using database/2 on DbConfig parameters. This 
compound terms has two arguments, the language name and the project name. For
example: `create_db(database(ada, myproject)).`.

```
?- create_db(database(ada, myproject)).
true

?- starting_with(database(ada, myproject), 'foo', LstRes).
LstRes=[ ... ]
```
*/

:- use_module(sqlite_common).
:- use_module(sqlite_decs).
:- use_module(sqlite_files).

:- ensure_loaded(library(prosqlite)).
:- ensure_loaded(library(db_facts)).

%-Section Database creation predicates

:- multifile constraints/2.

/**
 constraints(+DbConfig:term, +Term:term).

Can the given Term be added?

Use this predicate to check safely before adding a term into the database.

@param Term can be a dec/6 or file/1 term.
@param DbConfig is a database/2 term.
*/
constraints(_, _) :-
    db_disconnect(database),
    fail.

/** create_db(+DbConfig:term).

true iff can create a database with all its table for the given Language, or
true if the database is already created.

DbConfig is a compound term, database/2 as:
    database(+Language:atom, +DbName:atom).
*/    
create_db(DbConfig) :-
    lang_db_filename(DbConfig, DbFile),
    sqlite_connect(DbFile, database, exists(false)),

    create_declaration_table,
    create_file_table,

    db_disconnect(database).

%-Section Basic objects predicates.
%

%-Section Queries
%

/** delete_decs_file(+DbConfig:term, +FilePath: term) is det.

Remove all declarations and the file entry from the given File.

Return true even when no FilePath exists in the database. Only false when
the database doesn't exists.

Don't even think to rename this predicate to delete_file: it already exists!
*/
delete_decs_file(DbConfig, FilePath) :-
    delete_file(DbConfig, FilePath, _),
    delete_decs(DbConfig,
		dec(_Pack, _Name, _Args, _Desc, _Line, FilePath)).

prepare_list([], _DbConfig, _FilePath, []).
prepare_list([Dec|Rest], DbConfig, FilePath,
	     [fulldec(DbConfig, FilePath, Dec)| FullRest]) :-
    prepare_list(Rest, DbConfig, FilePath, FullRest).

/** save_all(+DbConfig:term, +File:term, +LstDecs:list).

Save declarations into the database selected by the DbConfig parameter.

File must be a file/2 term of format file(Path, Date). Date must be in seconds
since the Unix epoch.

@param LstDecs a list of terms dec(Pack, Name, Parameters, Desc, Line)
*/
save_all(DbConfig, file(FilePath, Date), LstDecs) :- 
    create_db(DbConfig),
    add_file(DbConfig, FilePath, Date),
    
    prepare_list(LstDecs, DbConfig, FilePath, LstDecs2),
    maplist(save_full_dec, LstDecs2).





