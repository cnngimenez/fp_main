/*   sqlite_decs.pl
     Author: cnngimenez.

     Copyright (C) 2020 cnngimenez

     This program is free software: you can redistribute it and/or modify
     it under the terms of the GNU General Public License as published by
     the Free Software Foundation, either version 3 of the License, or
     at your option) any later version.

     This program is distributed in the hope that it will be useful,
     but WITHOUT ANY WARRANTY; without even the implied warranty of
     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
     GNU General Public License for more details.

     You should have received a copy of the GNU General Public License
     along with this program.  If not, see <http://www.gnu.org/licenses/>.

     20 nov 2020
*/

:- module(sqlite_decs, [
	      create_declaration_table/0,

	      add_declaration/8,
	      add_declaration/3,

	      save_full_dec/1,

	      delete_decs/2,
	      
	      declaration/7,
	      all_declarations/8,

	      starting_with/3, has_substring/3
	  ]).

:- use_module(sqlite_common).
:- use_module(library(prosqlite)).
:- use_module(library(db_facts)).

:- multifile constraints/2.

/**
 constraints(+DbConfig:term, +Dec:term).

Can the given Dec declaration be added?

Use this term to check safely before adding a term into the database.

@param Dec is a dec/6 term.
@param DbConfig is a database/2 term.
*/
constraints(DbConfig, dec(_Pack, _Name, _Params, _Dec, Line, Filepath)) :-
    lang_db_filename(DbConfig, DbFile),
    sqlite_connect(DbFile, database),
    %% if db_holds/1 is true, then it already exists and it should fail.
    \+ db_holds(declarations_tbl(_, _, _, _, Line, Filepath)),
    db_disconnect(database),!.


/** create_declaration_table.

true iff can create the declaration table at the SQLite database for the
given Language database or if the table already exist. 
*/
create_declaration_table :-
    db_table(database, declarations_tbl),!.
create_declaration_table :-    
    db_create(database, declarations_tbl(pack-text, name-text, params-text,
                                         desc-text, line+int, file_path+text)).


delete_decs(DbConfig,
	    dec(Pack, Name, Args, Desc, Line, FilePath)) :-
    lang_db_filename(DbConfig, Dbfile),
    
    sqlite_connect(Dbfile, database),
    db_retractall(
	declarations_tbl(Pack, Name, Args, Desc, Line, FilePath)),
    db_disconnect(database).

/** row_to_dec(?LstRows, ?LstDecs).

true iff LstRows has all the row/5 predicates with the same information as the
dec/5 predicates of the list LstDecs.

Translate row/5 predicates from LstRows into dec/5 predicates to LstDecs, and
viceversa.
*/
row_to_dec([], []).
row_to_dec([row(Package, Name, Args, Desc, Line, File)|RRest],
           [dec(Package, Name, Args, Desc, Line, File)|DRest]) :-
    row_to_dec(RRest, DRest).


/** starting_with(+DbConfig:term, +Start:term, -LstRes:list).

true iff LstRes has all the dec/5 predicates obtained from the DB which name
starts with Start.
*/
starting_with(DbConfig, Start, LstRes) :-
    lang_db_filename(DbConfig, DbFile),
    sqlite_connect(DbFile, database),

    atomic_list_concat(
	['SELECT * FROM declarations_tbl WHERE name like "', Start, '%"'],
	Query),
    findall(Row, db_query(database, Query, Row), LstRow),
    row_to_dec(LstRow, LstRes),
    
    db_disconnect(database).

/** has_substring(+DbConfig:term, +Substring:term, -LstRes:list).

true iff LstRes has all the dec/6 predicates obtained from the DB which name
has the given substring.
*/
has_substring(DbConfig, Substring, LstRes) :-
    lang_db_filename(DbConfig, DbFile),
    sqlite_connect(DbFile, database),
    
    atomic_list_concat(
	['SELECT * FROM declarations_tbl WHERE name like "%', Substring, '%"'],
	Query),
    findall(Row, db_query(database, Query, Row), LstRow),
    row_to_dec(LstRow, LstRes),
    
    db_disconnect(database).

/** declaration(+DbConfig:term, ?Name:term, ?Parameters:term, ?Desc:term, ?Line:int, ?FilePath:term) is det

true iff declaration(Pa,N,P,D,L,F) exists on the database.
*/
declaration(DbConfig, Pack, Name, Parameters, Desc, Line, FilePath) :-
    lang_db_filename(DbConfig, DbFile),
    sqlite_connect(DbFile, database),
    
    ( db_holds(
	  declarations_tbl(Pack, Name, Parameters, Desc, Line, FilePath)), ! ;
      %% if db_holds/1 fails, disconnect and then redo.
      db_disconnect(database), fail), 

    db_disconnect(database).

add_declaration(DbConfig, Pack, Name, Parameters, Desc, Line, FilePath, Desc) :-
    constraints(DbConfig, dec(Pack, Name, Parameters, Desc, Line, FilePath)),
    
    lang_db_filename(DbConfig, DbFile),
    sqlite_connect(DbFile, database),
    db_assert(
	declarations_tbl(Pack, Name, Parameters, Desc, Line, FilePath)),!, 
    db_disconnect(database).

add_declaration(DbConfig, dec(Pack, Name, Parameters, Desc, Line), FilePath) :-
    constraints(DbConfig, dec(Pack, Name, Parameters, Desc, Line, FilePath)),
    
    lang_db_filename(DbConfig, DbFile),
    sqlite_connect(DbFile, database),
    db_assert(
	declarations_tbl(Pack, Name, Parameters, Desc, Line, FilePath)),!,
    db_disconnect(database).

/** all_declarations(+DbConfig:term, ?Name, ?Args, ?Desc, ?Line, ?FilePath, ?LstRes).

true iff LstRes is a list of dec/6 terms in the database that matchs the all of
the given Name, Args, Desc, Line and/or FilePath ( *if given instantiated* ).

*/
all_declarations(DbConfig, Pack, Name, Args, Desc, Line, FilePath, LstRes) :-
    lang_db_filename(DbConfig, DbFile),
    sqlite_connect(DbFile, database),
    
    findall(dec(Pack, Name, Args, Desc, Line, FilePath),
	    db_holds(declarations_tbl(Pack, Name, Args, Desc, Line, FilePath)),
	    LstRes),

    db_disconnect(database).


/** save_full_dec(+FullDec).

FullDec is a fulldec/3 predicate. Where:

    fulldec(+DbConfig:term, +FilePath:term, +Dec).

Where Dec is a dec/5 term
dec(+Pack: term, +Name:term, +Params:term, +Desc:term, +Line).

true whenever FullDec could be saved or not. 
*/
save_full_dec(fulldec(DbConfig, FilePath, Dec)) :-
    ( add_declaration(DbConfig, Dec, FilePath),! ; true).  
    
/** save_all_decs(+DbFile, +LstDecs:list, +FilePath:term).

Save a list of declarations terms. Used internally by save_all/3.

*Warning* : The database is expected to be opened!
*/
save_all_decs(_DbFile, [], _FilePath).
save_all_decs(DbFile, [dec(Pack, Name, Params, Desc, Line)|Rest], FilePath) :-
    sqlite_connect(DbFile, database),
    (db_holds(declarations_tbl(Pack, Name, Params, Desc, Line, FilePath)) ;
     db_disconnect(database), fail),
    db_disconnect(database),
    save_all_decs(DbFile, Rest, FilePath).
save_all_decs(DbFile, [dec(Pack, Name, Params, Desc, Line)|Rest], FilePath) :-
    sqlite_connect(DbFile, database),
    db_assert(declarations_tbl(Pack, Name, Params, Desc, Line, FilePath)),
    db_disconnect(database),
    save_all_decs(DbFile, Rest, FilePath).
