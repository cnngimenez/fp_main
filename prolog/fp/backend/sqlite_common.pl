/*   sqlite_common.pl
     Author: cnngimenez.

     Copyright (C) 2020 cnngimenez

     This program is free software: you can redistribute it and/or modify
     it under the terms of the GNU General Public License as published by
     the Free Software Foundation, either version 3 of the License, or
     at your option) any later version.

     This program is distributed in the hope that it will be useful,
     but WITHOUT ANY WARRANTY; without even the implied warranty of
     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
     GNU General Public License for more details.

     You should have received a copy of the GNU General Public License
     along with this program.  If not, see <http://www.gnu.org/licenses/>.

     20 nov 2020
*/


:- module(sqlite_common, [
	      lang_db_filename/2
	  ]).

fp_dir('~/.config/fp/').

/** lang_db_filename(DbConfig:term, DbFile:term).

true iff DbFile is the filepath for the given Language.

Remember to use `assertz(fp_dir('/path/here/')).' for configure where is
the databases.
*/
lang_db_filename(database(Language, DbName), DbFile) :-
    fp_dir(FPDir),
    format(atom(DbFile1), '~w/dbs/~w-~w.sqlite', [FPDir, Language,DbName]),
    expand_file_name(DbFile1, [DbFile]).

