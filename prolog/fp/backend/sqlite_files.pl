/*   sqlite_files.pl
     Author: cnngimenez.

     Copyright (C) 2020 cnngimenez

     This program is free software: you can redistribute it and/or modify
     it under the terms of the GNU General Public License as published by
     the Free Software Foundation, either version 3 of the License, or
     at your option) any later version.

     This program is distributed in the hope that it will be useful,
     but WITHOUT ANY WARRANTY; without even the implied warranty of
     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
     GNU General Public License for more details.

     You should have received a copy of the GNU General Public License
     along with this program.  If not, see <http://www.gnu.org/licenses/>.

     20 nov 2020
*/

:- module(sqlite_files, [
	      create_file_table/0,

	      file/3, all_files/2,

	      add_file/3,
	      delete_file/3,

	      file_unmodified/2	      
	  ]).

:- use_module(library(prosqlite)).
:- use_module(library(db_facts)).

:- use_module(sqlite_common).

:- multifile constraints/2.

/**
 constraints(+DbConfig:term, +File:term).

Can the given File be added?

File is a file/1 term. 
DbConfig is a database/2 term.

Use this term to check the file/1 term before adding it into the database.
*/
constraints(DbConfig, file(Path, Date)) :-
    lang_db_filename(DbConfig, DbFile),
    sqlite_connect(DbFile, database),
    %% if db_holds/1 is true, then it already exists and it should fail.
    \+ db_holds(files_tbl(Path, Date)), 
    db_disconnect(database),!. 


/** create_file_table.

true iff can create the file table at the SQLite database for the given
Language database
or if the table already exists.
*/
create_file_table :-
    db_table(database, files_tbl), !.
create_file_table :-
    db_create(database, files_tbl(path+text, lastseen-timestamp)).


/** file(+DbConfig:term, +Path:term, ?Date:int) is det

true iff file(P) exists on the database.

@param DbConfig database/2 term setting language and project name.
@param Path The absolute path inside the database.
@param Date Is expressed in seconds since the Unix epoch, can be a variable.
*/
file(DbConfig, Path, Date) :-
    lang_db_filename(DbConfig, DbFile),
    sqlite_connect(DbFile, database),

    ( db_holds(files_tbl(Path, Date)), ! ;
      %% if db_holds/1 fails, disconnects and then redo.
      db_disconnect(database), fail ),
    
    db_disconnect(database).

/**
   file_was_modified(+DbConfig:term, +FilePath:term).

True iff the file exists in the DB and it was not modified since the last scan.

This means, that the file exists, also there's a tuple on the database stored
with this FilePath and the date stored is different from the file's date.
*/
file_unmodified(DbConfig, FilePath) :-
    time_file(FilePath, FileTime),
    file(DbConfig, FilePath, TimeStored),!,
    FileTime =:= TimeStored.

/**
   all_files(+DbConfig:term, -LstFiles: list).

true iff LstFiles is a list of file/2 terms of all the files stored in the DB.

@param DbConfig A database/2 term, see create_db/1.
@param LstFiles A list of file(Path, Date) term.
*/
all_files(DbConfig, LstFiles) :-
    lang_db_filename(DbConfig, DbFile),
    sqlite_connect(DbFile, database),

    findall(file(Path, Date),
	    db_holds(files_tbl(Path, Date)),
	    LstFiles),
    
    db_disconnect(database).


/** add_file(+DbConfig:term, +File:term, +Date:int).

Add a File term into the database if it doesn't exists.

@param Date Must be a number of the seconds since the Unix epoch.
*/
add_file(DbConfig, File, Date) :-    
    constraints(DbConfig, file(File, Date)), !, % red cut

    lang_db_filename(DbConfig, DbFile),
    sqlite_connect(DbFile, database),    
    db_assert(files_tbl(File, Date)),
    db_disconnect(database).

add_file(_DbConfig, _File, _Date).


/**
 delete_file(+DbConfig:term, +FilePath:term, ?Date:int).

Remove files from the database.
*/
delete_file(DbConfig, FilePath, Date) :-
    lang_db_filename(DbConfig, Dbfile),
    
    sqlite_connect(Dbfile, database),    
    db_retractall(files_tbl(FilePath, Date)),
    db_disconnect(database).
