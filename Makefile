### Makefile --- 

## Author: cnngimenez
## Version: $Id: Makefile,v 0.0 2020/11/23 01:55:00  Exp $
## Keywords: 
## X-URL: 

# Copyright 2020 cnngimenez

# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.

# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.

# You should have received a copy of the GNU General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.

SHELL = /usr/bin/fish

all: compile

clean: bin/*.pl
	cd bin
	@echo rm (basename -s .pl $?)
	cd ..

compile: bin/*.pl
	cd bin
	@echo (basename -s .pl $?)
	swipl -o (basename -s .pl $?) -c $?
	cd ..

### Makefile ends here
